terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}


provider "azurerm" {
  skip_provider_registration = "true"
  features {}
}


resource "azurerm_resource_group" "ReGr1" {
  name     = "ReGr1"
  location = "West Europe"
  tags = {
    environment = "dev"
  }
}

resource "azurerm_virtual_network" "ViNe1" {
  name                = "ViNe1"
  resource_group_name = azurerm_resource_group.ReGr1.name
  location            = azurerm_resource_group.ReGr1.location
  address_space       = ["10.1.0.0/16"]

  tags = {
    environment = "dev"
  }
}

resource "azurerm_subnet" "Su1" {
  name                 = "Su1"
  resource_group_name  = azurerm_resource_group.ReGr1.name
  virtual_network_name = azurerm_virtual_network.ViNe1.name
  address_prefixes     = ["10.1.1.0/24"]
}

resource "azurerm_network_security_group" "SeGr1" {
  name                = "SeGr1"
  location            = azurerm_resource_group.ReGr1.location
  resource_group_name = azurerm_resource_group.ReGr1.name

  tags = {
    environment = "dev"
  }
}

resource "azurerm_network_security_rule" "SeRu1" {
  name                        = "SeRu1"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.ReGr1.name
  network_security_group_name = azurerm_network_security_group.SeGr1.name
}

resource "azurerm_subnet_network_security_group_association" "SeGrAs1" {
  subnet_id                 = azurerm_subnet.Su1.id
  network_security_group_id = azurerm_network_security_group.SeGr1.id
}

resource "azurerm_public_ip" "PuIP1" {
  name                = "PuIP1"
  resource_group_name = azurerm_resource_group.ReGr1.name
  location            = azurerm_resource_group.ReGr1.location
  allocation_method   = "Dynamic"

  tags = {
    environment = "dev"
  }
}

resource "azurerm_network_interface" "NeIn1" {
  name                = "NeIn1"
  location            = azurerm_resource_group.ReGr1.location
  resource_group_name = azurerm_resource_group.ReGr1.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.Su1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.PuIP1.id
  }

  tags = {
    environment = "dev"
  }
}

resource "azurerm_linux_virtual_machine" "LVM1" {
  name                  = "LiViMa1"
  resource_group_name   = azurerm_resource_group.ReGr1.name
  location              = azurerm_resource_group.ReGr1.location
  size                  = "Standard_B1ls"
  admin_username        = "adminuser"
  network_interface_ids = [azurerm_network_interface.NeIn1.id]

  custom_data = filebase64("customdata.tpl")

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/azurekey.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "20.04.202209200"
 }

 provisioner "local-exec" {
    command = templatefile("windows-ssh-script.tpl", {
      hostname = self.public_ip_address
      user = "adminuser",
      identityfile = "~/.ssh/azurekey"
    })
    interpreter = ["Powershell", "-Command"]
 }

 tags = {
  environment = "dev"
 }
}

data "azurerm_public_ip" "IpDa1" {
    name = azurerm_public_ip.PuIP1.name
    resource_group_name = azurerm_resource_group.ReGr1.name
}

output "public_ip_address" {
   value = "${azurerm_linux_virtual_machine.LVM1.name}: ${data.azurerm_public_ip.IpDa1.ip_address}" 
} 