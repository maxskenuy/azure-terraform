#!/bin/bash

# Fetch the host's SSH public key and add it to the known_hosts file
ssh-keyscan -p "$port" "$host" >> ~/.ssh/known_hosts

# Update package lists
sudo apt-get update

# Install prerequisite packages
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common gnupg-agent

# Add Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Add Docker repository
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu focal stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Update package lists with the Docker repository
sudo apt-get update

# Install Docker and Docker Compose
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Add current user to the 'docker' group to run Docker commands without sudo
sudo usermod -aG docker $USER

# Enable Docker to start on system boot
sudo systemctl enable docker

# Install Docker Compose
sudo curl -fsSL https://github.com/docker/compose/releases/latest/download/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose